package ru.pavlovka.nchomework3.exercise2;

import ru.pavlovka.nchomework3.utl.math.Angle;
import ru.pavlovka.nchomework3.utl.ui.ComandLine;

public class Exercise2 {

    public static void main(String[] args) {
        int rawAngle = ComandLine.nextInt("Enter angle: ");

        System.out.println("Normalized angle: ");
        System.out.println(Angle.normalizeAngle(rawAngle));
        System.out.println("Normalized angle FloorMod: ");
        System.out.println(Angle.normalizeAngleFloorMod(rawAngle));
    }
}
