package ru.pavlovka.nchomework3.exercise6;

import ru.pavlovka.nchomework3.utl.ui.ComandLine;
import ru.pavlovka.nchomework3.utl.math.Factorial;

public class Exercise6 {
    public static void main(String[] args) {
        int n = ComandLine.nextInt("factorial: ");
        System.out.println("n! = "+ Factorial.compute(n));
    }
}
