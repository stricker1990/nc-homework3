package ru.pavlovka.nchomework3.exercise4;

import ru.pavlovka.nchomework3.utl.ui.ComandLine;

public class Exercise4 {
    public static void main(String[] args) {
        double d = ComandLine.nextDouble();
        System.out.println(Math.nextUp(d));
        System.out.println(Math.nextDown(d));
    }
}
