package ru.pavlovka.nchomework3.exercise14;

import ru.pavlovka.nchomework3.utl.math.Matrix;
import ru.pavlovka.nchomework3.utl.ui.ComandLine;

import java.util.List;

public class Exercise14 {
    public static void main(String[] args) {
        List<List<Integer>> matrix = ComandLine.nextIntMatrix("Enter square:");
        System.out.println("Magic square: "+Matrix.isMagicSquare(matrix));
    }
}
