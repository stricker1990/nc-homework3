package ru.pavlovka.nchomework3.exercise8;

import ru.pavlovka.nchomework3.utl.ui.ComandLine;
import ru.pavlovka.nchomework3.utl.ui.Printer;

public class Exercise8 {
    public static void main(String[] args) {

        String s = ComandLine.nextString();

        System.out.println(s);

        Printer.printSubstrings(s);
    }
}
