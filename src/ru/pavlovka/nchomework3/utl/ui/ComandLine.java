package ru.pavlovka.nchomework3.utl.ui;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ComandLine {

    private static Scanner getScanner(){
        return new Scanner(System.in);
    }

    public static int nextInt(String question){
        System.out.println(question);
        Scanner scanner = getScanner();
        int result = scanner.nextInt();
        scanner.close();
        return result;
    }

    public static BigInteger nextBigInt(String question){
        System.out.println(question);

        Scanner scanner = getScanner();
        BigInteger result;

        result = scanner.nextBigInteger();

        scanner.close();

        return result;
    }

    public static BigInteger nextBigInt(BigInteger minValue, BigInteger maxValue, String question){

        BigInteger value;

        while(true) {
            value = nextBigInt(question);
            if(value.compareTo(BigInteger.valueOf(0))>=0
                    && value.compareTo(maxValue)<=0){
                break;
            }
            System.out.format("Введенное значение должно быть в диапазоне от %d до %d \n", minValue, maxValue);
        }
        return value;
    }

    public static BigInteger nextBigInt(BigInteger minValue, BigInteger maxValue){
        return nextBigInt(minValue, maxValue, "Enter Int: ");
    }

    public static int nextInt(){
        return nextInt("Enter Int:");
    }

    public static double nextDouble(String question){
        System.out.println(question);

        Scanner scanner = getScanner();

        double result = scanner.nextDouble();

        scanner.close();

        return result;
    }

    public static double nextDouble(){
        return nextDouble("Enter Double:");
    }

    public static String nextString(String question){
        System.out.println(question);
        String result = "";

        Scanner scanner = getScanner();
        if(scanner.hasNextLine()){
            result = scanner.nextLine();
        }

        scanner.close();

        return result;
    }

    public static String nextString(){
        return nextString("Enter String: ");
    }

    public static char[] nextChars(String question){
        return nextString(question).toCharArray();
    }

    public static char[] nextChars(){
        return nextChars("Enter String: ");
    }

    public static List<List<Integer>> nextIntMatrix(String question){
        System.out.println(question);
        Scanner scanner = getScanner();

        List<List<Integer>> result = new LinkedList<>();
        while (scanner.hasNextLine()){
            String line = scanner.nextLine();
            if(line.isEmpty()){
                break;
            }
            result.add(Arrays.asList(line.split(" ")).stream().map(Integer::parseInt).collect(Collectors.toList()));
        }
        return result;
    }
}
