package ru.pavlovka.nchomework3.utl.ui;

import java.math.BigInteger;
import java.util.*;

public class Printer {
    public static void printIntToBinary(int number){
        System.out.println(Integer.toBinaryString(number));
    }

    public static void printIntToOctal(int number){
        System.out.println(Integer.toOctalString(number));
    }

    public static void printIntToHex(int number){
        System.out.println(Integer.toHexString(number));
    }

    public static void printSum(BigInteger number1, BigInteger number2){
        BigInteger res = number1.add(number2);
        System.out.println("sum of "+number1+" and "+number2+" = "+res);
    }

    public static void printDifference(BigInteger number1, BigInteger number2){
        BigInteger res = number1.subtract(number2);
        System.out.println("deference of "+number1+" and "+number2+" = "+res);
    }

    public static void printProduct(BigInteger number1, BigInteger number2){
        BigInteger res = number1.multiply(number2);
        System.out.println("Product of "+number1+" and "+number2+" = "+res);
    }

    public static void printQuotient(BigInteger number1, BigInteger number2){
        BigInteger res = number1.divide(number2);
        System.out.println("Quotient of "+number1+" and "+number2+" = "+res);
    }

    public static void printRemainder(BigInteger number1, BigInteger number2){
        BigInteger res = number1.mod(number2                                                 );
        System.out.println("Remainder of "+number1+" and "+number2+" = "+res);
    }

    public static void printSubstrings(String string){
        for(int i=0; i<string.length(); i++){
            for(int j = i+1; j<string.length(); j++){
                String substring = string.substring(i, j).trim();
                if(!substring.isEmpty()) {
                    System.out.println(substring);
                }
            }
        }
    }

    public static void printStringsAreEquals(String string1, String string2){
        String result = "string \"" + string1 + "\", \"" + string2 + "\"";
        if(string1.equals(string2)){
            result += " equals";
        }else{
            result += " not equals";
        }
        result += " (with .equals()";
        System.out.println(result);

        result = "string \"" + string1 + "\", \"" + string2 + "\"";
        if(string1==string2){
            result += " equals";
        }else{
            result += " not equals";
        }
        result += " (with ==)";
        System.out.println(result);
    }

    public static void printSortedList(List<String> list){
        List<String> sortedList = new ArrayList<>(list);
        Collections.sort(sortedList);
        for(String string: sortedList){
            System.out.println(string);
        }
    }
}
