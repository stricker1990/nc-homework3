package ru.pavlovka.nchomework3.utl.math;

public class Angle {
    static private int MAX_DEGGREE = 359;

    public static int normalizeAngle(int angle){
        return angle % MAX_DEGGREE;
    }

    public static int normalizeAngleFloorMod(int angle){
        return Math.floorMod(angle, MAX_DEGGREE);
    }
}
