package ru.pavlovka.nchomework3.utl.math;

import java.util.List;

public class Matrix {

    public static boolean isMagicSquare(Integer[][] matrix){
        int squareSize = matrix.length;

        int mainDiagonalSum = 0;
        int collateralDiagonalSum = 0;

        int[] columnsSum = new int[squareSize];
        int[] rowsSum = new int[squareSize];

        for(int i=0; i<matrix.length; i++){
            if(matrix[i].length!=squareSize){
                return false;
            }
            for(int j=0; j<matrix[i].length; j++){
                rowsSum[i] += matrix[i][j];
                columnsSum[j] += matrix[i][j];

                if(i==j){
                    mainDiagonalSum +=matrix[i][j];
                }
                if(i==squareSize-j-1){
                    collateralDiagonalSum +=matrix[i][j];
                }
            }
        }

        if(mainDiagonalSum!=collateralDiagonalSum){
            return false;
        }

        int magicNumber =mainDiagonalSum;
        for(int i: rowsSum){
            if(i != magicNumber){
                return false;
            }
        }

        for(int i: columnsSum){
            if(i != magicNumber){
                return false;
            }
        }
        return true;
    }

    public static boolean isMagicSquare(List<List<Integer>> matrix){

        int matrixSize = matrix.size();

        int magicConstant = (int) (matrixSize*(Math.pow(matrixSize, 2)+1)/2);

        int mainDiagonalSum = 0;
        int collateralDiagonalSum = 0;

        int[] columnSum = new int[matrixSize];

        int rowIndex = 0;

        for(List<Integer> row: matrix){
            int rowSum = 0;
            if(row.size()!=matrixSize){
                return false;
            }
            int columnIndex = 0;
            for(int column: row){
                columnSum[columnIndex] += column;
                rowSum += column;

                if(rowIndex==columnIndex){
                    mainDiagonalSum += column;
                }
                if(rowIndex==matrixSize-columnIndex-1){
                    collateralDiagonalSum += column;
                }
                columnIndex++;
            }
            if(rowSum!=magicConstant){
                return false;
            }
            rowIndex++;
        }

        if(mainDiagonalSum!=magicConstant || collateralDiagonalSum!=magicConstant){
            return false;
        }
        for(int column: columnSum){
            if(column!=magicConstant){
                return false;
            }
        }
        return true;
    }

    public static Integer[][] toMatrixArray(List<List<Integer>> matrix){
        Integer[][] result = new Integer[matrix.size()][];
        for(int i=0; i<matrix.size(); i++){
            List<Integer> row = matrix.get(i);
            result[i] = row.toArray(new Integer[row.size()]);
        }
        return result;
    }
}
