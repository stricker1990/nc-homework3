package ru.pavlovka.nchomework3.utl.math;

public class MyMath {
    public static int max(int val1, int val2, int val3){

        int max = Integer.MIN_VALUE;

        if(val1>max){
            max = val1;
        }

        if(val2>max){
            max =  val2;
        }

        if(val3>max){
            max = val3;
        }
        return max;
    }
}
