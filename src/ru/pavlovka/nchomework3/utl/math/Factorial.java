package ru.pavlovka.nchomework3.utl.math;

import java.math.BigInteger;
import java.util.Objects;

public class Factorial {
    private int n;

    public static BigInteger compute(int n){
        BigInteger result = BigInteger.valueOf(1);

        for(int i=1; i<=n; i++){
            result = result.multiply(BigInteger.valueOf(i));
        }
        return result;
    }

    public Factorial(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public BigInteger compute(){

        return Factorial.compute(this.n);
    }

    @Override
    public String toString() {
        return ""+n+"!";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Factorial factorial = (Factorial) o;
        return n == factorial.n;
    }

    @Override
    public int hashCode() {
        return Objects.hash(n);
    }
}
