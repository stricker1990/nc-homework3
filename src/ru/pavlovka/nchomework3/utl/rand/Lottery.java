package ru.pavlovka.nchomework3.utl.rand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Lottery {

    public static List<String> getRandomTickets(int startSequency, int endSequency, int ticketSize, int ticketsCount){
        List<String> list = new ArrayList<>(ticketsCount);
        Lottery lottery = new Lottery(startSequency, endSequency);
        for(int i=0; i<ticketsCount; i++){
            list.add(lottery.nextTicket(ticketSize));
        }
        return list;
    }

    private List<Integer> numbers;

    private Random random;

    Lottery(List<Integer> numbers){
        reset(numbers);
    }

    Lottery(int start, int end){
        reset(start, end);
    }

    Lottery(int end){
        reset(end);
    }

    public void reset(List<Integer> numbers){
        this.numbers = new ArrayList<>(numbers);
        random = new Random(System.currentTimeMillis());
    }

    public void reset(int start, int end){
        start = Math.min(start, end);
        end = Math.max(start, end);
        List<Integer> list = new ArrayList<>(end-start-1);
        for(int i=start; i<=end; i++){
            list.add(i);
        }
        reset(list);
    }

    public void reset(int end){
        reset(0, end);
    }

    public Integer next(){
        Integer randElement = MyRandom.randomElement(numbers);
        if(randElement != null){
            numbers.remove(randElement);
        }
        return randElement;
    }

    public String nextTicket(int ticketSize){
        ticketSize = Math.min(ticketSize, numbers.size());
        StringBuilder builder =new StringBuilder();
        for(int i=0; i<ticketSize; i++){
            Integer nextNumber = next();
            if(nextNumber==null){
                break;
            }
            builder.append(nextNumber);
            builder.append(" ");
        }
        return builder.toString();
    }
}
