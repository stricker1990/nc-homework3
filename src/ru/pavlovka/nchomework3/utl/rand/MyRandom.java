package ru.pavlovka.nchomework3.utl.rand;

import java.util.List;
import java.util.Random;

public class MyRandom {
    public static String randomBase36(){
        Random random = new Random(System.currentTimeMillis());
        long randomLong = random.nextLong();
        return Long.toString(randomLong, 36);
    }

    public static Integer randomElement(List<Integer> list, Random random){
        if(list.size()==0){
            return null;
        }
        return list.get(random.nextInt(list.size()));
    }

    public static Integer randomElement(List<Integer> list){
        return randomElement(list, new Random(System.currentTimeMillis()));
    }
}
