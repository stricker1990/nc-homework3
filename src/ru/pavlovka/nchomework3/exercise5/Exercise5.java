package ru.pavlovka.nchomework3.exercise5;

public class Exercise5 {
    public static void main(String[] args) {
        double d = Double.MAX_VALUE;
        System.out.println("max Double value "+d);
        System.out.println("casted to int "+(int) d);

        System.out.println("max int value "+Integer.MAX_VALUE);
    }
}
