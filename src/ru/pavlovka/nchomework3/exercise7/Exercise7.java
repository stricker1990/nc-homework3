package ru.pavlovka.nchomework3.exercise7;

import ru.pavlovka.nchomework3.utl.ui.ComandLine;
import ru.pavlovka.nchomework3.utl.ui.Printer;

import java.math.BigInteger;

public class Exercise7 {

    public static void main(String[] args) {

        BigInteger number1 = ComandLine.nextBigInt(BigInteger.valueOf(0), BigInteger.valueOf(4294967295L), "Enter number 1: ");
        BigInteger number2 = ComandLine.nextBigInt(BigInteger.valueOf(0), BigInteger.valueOf(4294967295L), "enter number 2: ");

        Printer.printSum(number1, number2);
        Printer.printDifference(number1, number2);
        Printer.printProduct(number1, number2);
        Printer.printQuotient(number1, number2);
        Printer.printRemainder(number1, number2);

    }
}
