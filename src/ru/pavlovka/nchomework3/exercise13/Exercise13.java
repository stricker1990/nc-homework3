package ru.pavlovka.nchomework3.exercise13;

import ru.pavlovka.nchomework3.utl.rand.Lottery;
import ru.pavlovka.nchomework3.utl.ui.Printer;

import java.util.Collections;
import java.util.List;

public class Exercise13 {
    public static void main(String[] args) {
        List<String> list = Lottery.getRandomTickets(1, 49, 6, 6);
        Printer.printSortedList(list);
    }
}
