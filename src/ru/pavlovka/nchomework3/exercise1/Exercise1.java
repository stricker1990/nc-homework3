package ru.pavlovka.nchomework3.exercise1;

import ru.pavlovka.nchomework3.utl.ui.ComandLine;
import ru.pavlovka.nchomework3.utl.ui.Printer;

public class Exercise1 {

    public static void main(String[] args) {
        int number = ComandLine.nextInt();
        Printer.printIntToBinary(number);
        Printer.printIntToOctal(number);
        Printer.printIntToHex(number);
    }
}
