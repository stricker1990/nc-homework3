package ru.pavlovka.nchomework3.exercise3;

import ru.pavlovka.nchomework3.utl.math.MyMath;
import ru.pavlovka.nchomework3.utl.ui.ComandLine;

public class Exercise3 {

    public static void main(String[] args) {
        int n1 = ComandLine.nextInt("First number:");
        int n2 = ComandLine.nextInt("Second number:");
        int n3 = ComandLine.nextInt("Three number:");

        int max = MyMath.max(n1, n2, n3);

        System.out.println("max (conditional operator)");
        System.out.println(max);

        System.out.println("max (Math.max)");
        System.out.println(Math.max(n1, Math.max(n2, n3)));
    }
}
