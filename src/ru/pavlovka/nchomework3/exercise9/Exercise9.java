package ru.pavlovka.nchomework3.exercise9;

import ru.pavlovka.nchomework3.utl.ui.ComandLine;
import ru.pavlovka.nchomework3.utl.ui.Printer;

public class Exercise9 {
    public static void main(String[] args) {
        String string1 = ComandLine.nextString();
        String string2 = new String(string1.getBytes());

        Printer.printStringsAreEquals(string1, string2);
    }
}
